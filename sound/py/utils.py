""" Useful? functions """


def parse_msg(msg):
    """ Parse the args message from Supercollider

    message format: [keyPitchClass ^ melPitchClasses ^ rhythmVector]
    where melPitchClasses is a comma-separated list of pitch classes of melody notes
    and rhythmVector is a length 16 list of 0's or 1's (where 1 indicates note event)

    example message:
    [ 19, "^", 10.0, 6.0, 10.0, 5.0,
    "^",
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0
    ]

    """
    # msg = msg[0]
    msg = msg.strip()
    msg = msg.replace('[','')
    msg = msg.replace(']','')
    msg = msg.replace('"','')

    key,melody_data,rhythm_data = msg.split('^')

    # hack to get rid of extraneous commas
    key = [a.strip() for a in key.split(',') if not a.strip() == '']
    key = int(float(key[0])) % 12

    melody_data = [a.strip() for a in melody_data.split(',') if not a.strip() == '']
    melody_list = [int(float(a)) for a in melody_data]

    rhythm_data = [a.strip() for a in rhythm_data.split(',') if not a.strip() == '']
    rhythm_list = [int(float(a)) for a in rhythm_data]

    return key,melody_list,rhythm_list

def rhythm_vector_to_times(rhythm_vector,subdiv=0.25):
    """ convert a rhythm vector to times """

    idx = [i for i,x in enumerate(rhythm_vector) if x == 1]
    start_times = [subdiv*i for i in idx]
    end_times = start_times[1:]
    end_times.append(subdiv*(len(rhythm_vector)+1))

    return start_times,end_times

