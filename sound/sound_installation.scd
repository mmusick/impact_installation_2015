/**************************************************************

 Project: impact_installation_2015
    File: sound_installation.scd

  Author: Michael Musick
   Email: michael@michaelmusick.com

 Created: 2015-07-26 11:23:55
Modified: 2015-09-15 16:32:13


   Notes:


**************************************************************/

(
s.recChannels = 8;
s.recSampleFormat = 'int24';
s.record;
)



Server.killAll;
(
o = Server.default.options;
o.memSize = 2**20;
o.numInputBusChannels  = 16;
o.numOutputBusChannels = 8;
o.device = "Avid 002 Rack";
// o.inDevice = "Built-in Microph";
// o.outDevice = "Built-in Output";
s.reboot;
);

(
s.makeWindow;
s.meter;
s.plotTree;
)


/**************************************************************************/
(
s.volume = -3.5;
Buffer.freeAll;
~path = Platform.userHomeDir ++ "/creative/impact_installation_2015/sound/";
// ~maxAddr = NetAddr("127.0.0.1", 7401);
~maxAddr = NetAddr("172.22.35.104", 7401);

~restRatio = 0.95;
~grainWait = 4;

~impact = ();
~impact.micChans = [8,9,10];
~impact.numMics = if(~impact.micChans.size>1, {~impact.micChans.size},{1});
~impact.speakerChan = [0,1,2,3,4];
~impact.numSpeakers = if(~impact.speakerChan.size>1, {~impact.speakerChan.size},{1});
~impact.outBus = Bus.audio(s, ~impact.numSpeakers);
~impact.restBus = Bus.control(s, 1);
~impact.grainBus = Bus.control(s, 1);

// Python Addr
~oscAddr = NetAddr.new( "127.0.0.1", 9000);
// Start Python
("python" + ~path++"py/chordgenerator.py").runInTerminal;
CmdPeriod.doOnce({"killall -v SIGKILL Python".unixCmd;});


~addr = NetAddr.localAddr;

(~path++"toLoad/*.scd").loadPaths;


~impact.numMics.do({
	arg id;
	Synth(\reflectiveMomentsAnalysis, [
		\micBus, ~rm_info.listener[id].micSig.index,
		\ampBus, ~rm_info.listener[id].ampSig.index,
		\krBus, ~rm_info.listener[id].mfccSig.index,
		\fluxBus, ~rm_info.listener[id].fluxBus.index,
		\onsetThresh, 0.4,
		\input, ~impact.micChans[id],
		\id, id
	]);
});


Pdefn(\out1, ~impact.outBus.index + ( 0 % ~impact.numSpeakers ));
Pdefn(\out2, ~impact.outBus.index + ( 2 % ~impact.numSpeakers ));
Pdefn(\out3, ~impact.outBus.index + ( 3 % ~impact.numSpeakers ));
Pdefn(\out4, ~impact.outBus.index + ( 4 % ~impact.numSpeakers ));
Pdefn(\outB, ~impact.outBus.index + ( 1 % ~impact.numSpeakers ));
Pdef(\voice1).quant_(8).align([4, 0, 0]);
Pdef(\voice2).quant_(8).align([4, 0, 0]);
Pdef(\voice3).quant_(8).align([4, 0, 0]);
Pdef(\voice4).quant_(8).align([4, 0, 0]);
Pdef(\voiceB).quant_(8).align([4, 0, 0]);
// comment out any voices to reduce number
Pdef(\voice1).play;
Pdef(\voice2).play;
Pdef(\voice3).play;
Pdef(\voice4).play;
Pdef(\voiceB).play;





~sysOut = SynthDef(\masterOut, {
	arg input = 0, output = 0;
	var sig;

	sig = InFeedback.ar( input, ~impact.numSpeakers);

	sig = HPF.ar(sig, 30);
	sig = LowShelf.ar(sig, 88, dbgain: -6);
	sig = Limiter.ar(sig, 0.98, 0.1);

	Out.ar( ~impact.speakerChan[0], sig[0] );
	Out.ar( ~impact.speakerChan[1], sig[2] );
	Out.ar( ~impact.speakerChan[2], sig[1] );
	Out.ar( ~impact.speakerChan[3], sig[3] );
	Out.ar( ~impact.speakerChan[4], sig[4] );
}).play(s,
	args: [\input, ~impact.outBus],
	addAction: \addToTail
);



// Task to send to MAX
~maxTask = Task({

	inf.do({
		{
			~rm_info.listener[0].ampSig.get({arg val; ~maxAddr.sendMsg('/amp0', val);});
			~rm_info.listener[1].ampSig.get({arg val; ~maxAddr.sendMsg('/amp1', val);});
			~rm_info.listener[2].ampSig.get({arg val; ~maxAddr.sendMsg('/amp2', val);});
			~pitchBus.get({arg val; ~maxAddr.sendMsg('/freq', val[0]);});
			~tempoBus.get({arg val; ~maxAddr.sendMsg('/tempo', val[0]);});
		}.fork;
		0.2.wait;
	});
}).start;

OSCdef.newMatching(\changeRatios,
	{
		var restRatio, grainWait;

		grainWait = 4.0.rand;
		grainWait.postln;
		restRatio = (1.0.rand).linlin(0, 1, 0.5, 1);
		restRatio.postln;

		~ratioSynth.set(\restRatio, restRatio, \grainWait, grainWait);
	},
	'/changeRatios'
);
~ratioTask = Task({
	inf.do({
		~impact.restBus.get({arg val; ~restRatio = val; });
		~impact.grainBus.get({arg val; ~grainWait = val; });
		0.1.wait;
	});
}).start;


~ratioSynth = SynthDef(\ratioSynth, {
	arg restRatio = 0.9, grainWait = 4, lagTime = 10, restOut = 0, grainOut = 1;
	var restSig, grainSig;

	restSig = restRatio;
	restSig = restSig.lag(lagTime);
	Out.kr(restOut, restSig);

	grainSig = grainWait;
	grainSig = grainSig.lag(lagTime);
	Out.kr(grainOut, grainSig);

}).play( args: [\restOut, ~impact.restBus, \grainOut, ~impact.grainBus]);

SynthDef(\recordOutput, {
	Out.ar(5, SoundIn.ar(8));
	Out.ar(6, SoundIn.ar(9));
	Out.ar(7, SoundIn.ar(10));
}).play;



)