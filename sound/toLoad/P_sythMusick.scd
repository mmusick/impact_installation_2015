/**********************************************************/
/*

Beautiful Interactive MusiCk forSyth's

P - Module
This talks to the 'f' module in Python through OSC


Initial SC build

M. Musick

*/
/**********************************************************/





/**********************************************************/
// INSTRUCTIONS
// 1. Initialize the Buses, Buffers, and Enviornment Variables/Array's
// 		(This only needs to be done once after starting the Server)
// 2. Execute the code Below to start the System
//





/**********************************************************/
/* I. -- START YOUR ENGINES!!!!   */




/**********************************************************/
/* I. -- INITIALIZATION   */

/* Initialize Buses and other Enviornment Variables */
(
"\'P_SythMusick Loading\'".postln;
// ENVIORNMENT VARIABLES
// Number of beats between note frame sends to pyFST
// ~micIn = [0, 1];
~micIn = 10;
~numOfChannels = 2;
~noteWindowLength = 8;
~rhythmSubdivision = 4;
~noteLenMin = 0.2;
~numOfMics = (~micIn.size).clip(1, 8);

// CONTROL BUSES
// Chroma Bus
~chromaBus = Bus.control(s, 12);
// Pitch Bus (order is as follows)
// [freq, onsetFreq, delayFreq, chromaMax, currentKey[1], hasFreq.lag(0.1)]
~pitchBus = Bus.control(s, 6);
// tempo bus
~tempoBus = Bus.control( s, numChannels: 2 );

// AUDIO BUSES
// delayed mic signal
~micDelayed = Bus.audio(s, ~numOfMics);
// audio feedback bus
~audioFeedback = Bus.audio(s, 1);

// BUFFER ARRAYS
// Recorded notes buffer
~buffNotes = Array.newClear( 128 );
// temporary recording buffer
~tempNoteBuff = Array.fill( 4, {
	arg env;
	env = ();
	// create a buff instance variable and allocate buffer
	env.buff = Buffer.alloc(s, s.sampleRate * 3.2, 1 );
	// create a 'busy' instance variable
	env.busy = false;
}; );
~tempID = 0;



//OSC ADDRESSES
// Python Addr

);


/**********************************************************/
/* II. -- START THE PIECE    */
(

// OSC RESPONDERS

/* 1. noteOnset Responder */
// This recieves messages from the analyzer synth when ever a
// note onset is detected. This will build the "note array"
// that is sent to Python.
// It also initiates the recording of notes for the synthesis module
(
// reset global enviornment variables
// ~buffNotes.do({
// 	arg i, idx;
// 	~buffNotes[idx].free;
// 	~buffNotes[idx] = nil;
// });
~tempID = 0;
~noteArray = [];
~rhythmArray = (
	ptr: 0,
	len: (~noteWindowLength * ~rhythmSubdivision),
	arr: Array.fill( (~noteWindowLength * ~rhythmSubdivision), {0}),
	sub: ~rhythmSubdivision
);
~noteOnsetResponder.free;
// free the busy signals
~tempNoteBuff.do( {|item, index| item.busy = false;}; );

~noteOnsetResponder = OSCFunc(
	{
		arg msg, time, addr, recvPort;
		var note, validData;
		// msg.postln;

		// get the note from the OSC msg
		note = msg[3];

		// Check that this is a valid number
		if( (note.isNumber).and(note >= 0).and(note < 128), {
				validData=true;
				// append the note to the note array
				// "% 12" brings all values between 0-11
				~noteArray = ~noteArray ++ [ (note%12) ];
				// ~noteArray.postln;
			},{
				validData=false;
		});

		// check if the tempBuff is busy
		// this is where notes get recorded from the room mic
		if( validData.and(~tempNoteBuff[~tempID].busy),
			{},
			{
				// "".postln;
				// (
				// 	"Recording temp note: " ++ note.asString ++
				// 	" in temp: " ++ ~tempID
				// ).postln;

				// zero the temp buff to prepare it
				~tempNoteBuff[~tempID].buff.zero;
				// record that sucker using the noteRecorder Synth
				Synth(\noteRecorder,
					[
						\recordBuffnum, ~tempNoteBuff[~tempID].buff,
						\id, ~tempID,
						\note, note
					],
					// \addToTail places it after the analyzer/mic synth
					addAction: \addToTail
				);
				// mark this as busy
				~tempNoteBuff[~tempID].busy = true;
				// increment the tempID pointer
				~tempID = (~tempID+1)%4;
		});

		// mark the Rhythm Tracker
		if(~rhythmArray.ptr < ~rhythmArray.len,
			{ ~rhythmArray.arr[~rhythmArray.ptr] = 1; };
		);
	},
	'/noteOnset'
);
);


/* 2. send key and pitches to python */
// This process prepends the key estimate to the note array
// keys are represented as values 0-23
// after sending, array is array
// this is all controlled by the found tempo
(
~arraySendTask.stop;
~arraySendTask = Task({
	inf.do({
		var key, array;
		// msg.postln;
		if( ~noteArray.size > 0, {
			// get the key from the pitch bus
			key = (~pitchBus.getnSynchronous(6))[4];
			// prepend it to the note array
			array = [key.asInteger] ++ ["^"];
			array = array ++ ~noteArray ++ ["^"];
			array = array ++ ~rhythmArray.arr;
			// send it to Python
			~oscAddr.sendMsg("/sc2py", array.asCompileString);
			// Test post
			// "Sent the following note array to Python: ".post;
			// array.postln;
		});

		// clear the note array
		~noteArray = [];

		// reset the rhythm array
		// ~rhythmArray.postln;
		~rhythmArray.ptr = 0;
		~rhythmArray.sub = ~rhythmSubdivision;
		~rhythmArray.len = (~noteWindowLength * ~rhythmArray.sub);
		~rhythmArray.arr = Array.fill( ~rhythmArray.len, {0});
		~noteWindowLength.wait;
	});
	},
	// By setting it to TempoClock,
	// the task remains tied to the beat/tempo
	TempoClock
).start( quant: 1 );
);


/* 3. Copy Temp Buffer to noteArray[] buffer */
(
~noteCopy.free;
~noteCopy = OSCFunc(
	{
		arg msg, time, addr, recvPort;
		var id, note, length, ampAvg;
		var copy = true, validData = false;
		id = msg[3];
		note = msg[4].asInteger;
		length = msg[5];
		ampAvg = msg[6];

		// ("Note is over. Checking copy status").postln;
		// ("Note: "++note++" tempID: "++id++" length: "++length).postln;

		if( (note >= 0).and(note < 128),
			{validData=true}, {validData=false}
		);

		// note.postln;
		// ~buffNotes[note].postln;
		// should we copy it into the note buffer?
		if( (~buffNotes[note].notNil),
			{
				// If there is something already there,
				// than it will be replaced if the new sample
				// is longer than 1 second.
				// Check that it is at least 1 second long
				if( (length < (s.sampleRate * ~noteLenMin)).and(ampAvg<0.002),
					{copy=false}, {copy=true;} );
			},{
				// if the ~buffNote pos was nil then add it,
				// cause we need something
				if( (length < (0.1*s.sampleRate)).and(ampAvg<0.002),
					{copy=false}, {copy=true;} );
			}
		);
		// Try and copy it over
		if( copy.and(validData),
			{
				// make sure everything is free'd and clear
				~buffNotes[note].free;
				~buffNotes[note] = nil;

				// normalize to 1.0
				~tempNoteBuff[id].buff.normalize;
				// allocate a new buffer of the correct length.
				~buffNotes[note] = Buffer.alloc(
					s,
					length.asInteger,
					1,
					// send a message which calls a copy function once its allocated
					completionMessage: { ~addr.sendMsg('/copyReady', note, id); }
				);
			},{
				// ("Will not copy note: "++note).postln;
				// if not set the busy tag to false
				~tempNoteBuff[id].busy = false;
			}
		);
		// [id, note, length].postln;
	},
	'/noteData'
);
);


/* BUFFER READY FOR COPY */
// This is called when the above audio buffer has been allocated
(
~copyReady.free;
~copyReady = OSCFunc(
	{
		arg msg, time, addr, recvPort;
		var note, id;
		// msg.postln;
		// set the note and id var's'
		note = msg[1].asInteger;
		id = msg[2];
		// copy the data into ~buffNotes Array!
		~tempNoteBuff[id].buff.copyData(~buffNotes[note]);

		// let the system know the temp buff is ready for a new recording
		~tempNoteBuff[id].busy = false;

		// ("Copied note: "++note++" into buffer "++~buffNotes[note].bufnum++
		// 	" and freed Temp buff "++id).postln;
	},
	'/copyReady'
);
);


/* Rhythm Tracker */
(
~rhythmTracker.stop;
~rhythmTracker = Task({
	inf.do({
		// Increament the rhythm ptr every sixteenth note
		~rhythmArray.ptr = ~rhythmArray.ptr + 1;
		// ~rhythmArray.ptr.postln;
		(1/~rhythmArray.sub).wait;
	});

},
TempoClock
).start( quant: 1);
);


// NOTE RECORDER SYNTHDEF
(
SynthDef(\noteRecorder,
	{
		arg in = 0, recordBuffnum = 0, note = 60, t_reset = 0, gate = 1, id = 0,
			shiftWindowSize = 0.05;
		var sig, freq, ratio, ratioCheck, ampAvg;
		var playhead_ar, recorder, envTrig, env, countdown;

		// get the signal from the delayed mic
		sig = In.ar( ~micDelayed, 1 );
		// sig = SoundIn.ar(0);

		// get the frequency and certaintyMeasure from the analyzer
		// if these deviate too far than stop the recorded
		freq = In.kr(~pitchBus, 6);
		// delayFreq comes in on 3 [2]
		freq = freq[2];
		// freq.poll(label: "freq");

		// find the difference between the target freq
		// and analyzed freq
		ratio = note.midicps / freq;
		// ratio.poll(label: "ratio");
		// shift the frequency
		sig = PitchShift.ar(
			in: sig,
			windowSize: shiftWindowSize,
			pitchRatio: ratio,
			pitchDispersion: 0,
			timeDispersion: 0.005
			// timeDispersion: ratio
			// .linlin( 20, 100, 0.0001, shiftWindowSize )
			// .clip( 0, shiftWindowSize )
		);
		// check ratio range
		// if it goes outside of bounds then stop recorder
		ratioCheck = InRange.kr( ratio, 0.92, 1.15);
		// ratioCheck.poll(label: "ratioCheck");
		// get playhead marker for bufWr
		playhead_ar = Phasor.ar(
			trig: t_reset,
			rate: 1,
			start: 0,
			end: BufFrames.kr(recordBuffnum),
			resetPos: (s.sampleRate * 3 )-1
		);
		// playhead_ar.poll(label: \playhead);

		// keep track of space remaining in each buffer
		countdown = (playhead_ar-BufFrames.kr(recordBuffnum));
		// countdown.poll(label: \countdown);
		// control envelope
		envTrig = (
			(
				gate *
				ratioCheck *
				(2 - Trig1.kr( (countdown+(s.sampleRate*0.11))))
			) + Line.kr(1, 0, 0.2)
		);
		env = Env.new([0,0,1,1,0,0], [0.001, 0.01, 0, 0.01, 0.01], releaseNode: 3 );
		env = EnvGen.ar(env, envTrig );
		// env.poll(label: \env);

		recorder = BufWr.ar( sig*env, recordBuffnum, playhead_ar, 0 );
		// recorder.poll(label: \recorder);

		ampAvg = RunningSum.rms(sig, s.sampleRate);
		// ampAvg.poll(label: \rms1);

		// if at the end, then let client know and relay the playhead positions
		SendReply.ar(
			trig: [K2A.ar(Done.kr(env)), countdown, K2A.ar(Done.kr(recorder))],
			cmdName: '/noteData',
			values: [id, note, playhead_ar, ampAvg],
			replyID: 1
		);
		FreeSelf.kr( (Done.kr(recorder) + K2A.ar(Done.kr(env))) );
	}
).add;
);





// ANALYZER SYNTHDEF
// This SynthDef analyzes the incoming audio stream for features
// These are then placed in buses and passed around the system
// This is started right away and runs throughout the piece

(
~analyzer = SynthDef( \analyzer, {
	arg outBus = 0, onsetDelay = 0.05, onsetThresh = 0.2, tempoLock = 0,
		internalFB_lag = 1, internalFB_amount = 1;
	var inSig, out, freq, hasFreq, onsetFreq, delayFreq, chroma, chromaMaxVal, chromaMax;
	var onsets, onsetTrig, beep, key, currentKey, flux, fft;
	var quarter, eighth, sixt, tempo, sendTrig;
	var rms, feedbackAudioSig;

	// pull in sound from the world!
	inSig = SoundIn.ar( ~micIn );

	inSig = Mix(inSig);

	// get rms in order to gate feedback audio
	rms = RunningSum.rms( inSig, 2048 ).lag(0.1, internalFB_lag);
	rms = (rms - 1).abs;
	rms = rms * internalFB_amount;

	// feedbackAudioSig = In.ar( ~audioFeedback, 1 );
	// feedbackAudioSig = feedbackAudioSig * Linen.kr( (rms - 0.97), 1 );

	// mix the two together
	// inSig = Mix([feedbackAudioSig, inSig]);

	// get fundamental frequency estimation
	// and certainty measure
	#freq, hasFreq = Pitch.kr(
		in: inSig,
		initFreq: 200,
		minFreq: 60,
		maxFreq: 4000,
		median: 5,
		ampThreshold: 0.01,
		peakThreshold: 0.5,
		clar: 1
	);
	// freq.poll(label: \freq);
	// hasFreq.poll(label: \frequencyCertainty);
	// only use frequency when certainty is high
	freq = Gate.kr( freq, (hasFreq - 0.7) );

	// get chroma array
	chroma = Chromagram.kr(
		fft: FFT(LocalBuf(2048), inSig),
		fftsize: 2048,
		// integrationflag: 0,
		// perframenormalize: 1
		integrationflag: 1
	);
	// get the largest chroma note value
	#chromaMaxVal, chromaMax  = ArrayMax.kr(chroma);
	// chroma.poll(label: \chroma);

	// get a key estimate
	key = KeyTrack.kr(
		chain: FFT( LocalBuf(4096), inSig ),
		keydecay: 2,
		chromaleak: 0.5
	);
	// key.poll;
	// look for the most frequently occuring key over a 10" lag
	currentKey = Array.newClear(24);
	currentKey[0] = InRange.kr( key, 0, 0 ).lag(10);
	currentKey[1] = InRange.kr( key, 1, 1 ).lag(10);
	currentKey[2] = InRange.kr( key, 2, 2 ).lag(10);
	currentKey[3] = InRange.kr( key, 3, 3 ).lag(10);
	currentKey[4] = InRange.kr( key, 4, 4 ).lag(10);
	currentKey[5] = InRange.kr( key, 5, 5 ).lag(10);
	currentKey[6] = InRange.kr( key, 6, 6 ).lag(10);
	currentKey[7] = InRange.kr( key, 7, 7 ).lag(10);
	currentKey[8] = InRange.kr( key, 8, 8 ).lag(10);
	currentKey[9] = InRange.kr( key, 9, 9 ).lag(10);
	currentKey[10] = InRange.kr( key, 10, 10 ).lag(10);
	currentKey[11] = InRange.kr( key, 11, 11 ).lag(10);
	currentKey[12] = InRange.kr( key, 12, 12 ).lag(10);
	currentKey[13] = InRange.kr( key, 13, 13 ).lag(10);
	currentKey[14] = InRange.kr( key, 14, 14 ).lag(10);
	currentKey[15] = InRange.kr( key, 15, 15 ).lag(10);
	currentKey[16] = InRange.kr( key, 16, 16 ).lag(10);
	currentKey[17] = InRange.kr( key, 17, 17 ).lag(10);
	currentKey[18] = InRange.kr( key, 18, 18 ).lag(10);
	currentKey[19] = InRange.kr( key, 19, 19 ).lag(10);
	currentKey[20] = InRange.kr( key, 20, 20 ).lag(10);
	currentKey[21] = InRange.kr( key, 21, 21 ).lag(10);
	currentKey[22] = InRange.kr( key, 22, 22 ).lag(10);
	currentKey[23] = InRange.kr( key, 23, 23 ).lag(10);
	// currentKey.poll;
	// find the best key estimate in the array by looking for Array Max
	currentKey = ArrayMax.kr( currentKey );
	// this is placed into a new array of [value, pos]
	// currentKey.poll;

	// get flux
	fft = FFT(
		buffer: LocalBuf(1024),
		in: inSig,
		hop: 0.5,
		wintype: 1 /*hann*/
	);
	flux = FFTFlux.kr( fft );
	// flux.poll(label: \flux);

	// get Onsets
	onsets = Onsets.kr(
		chain: fft,
		threshold: onsetThresh,
		odftype: \complex,
		relaxtime: 1,
		floor: 0.1,
		mingap: 10,
		medianspan: 11
	);
	// delay the onsets by x-time so that freq is accurate
	onsetTrig = DelayN.kr( onsets, maxdelaytime: onsetDelay, delaytime: onsetDelay );

	// only trigger when flux is stable
	// this serves as a measure as to the kind of activity occuring in the space
	onsetTrig = Gate.kr(
		onsetTrig,
		InRange.kr( flux, 0.0, 0.2 ) - 0.999
	);
	// onsetTrig.poll(label: \onsetSig);

	// only update freq on "note onset"
	onsetFreq = Gate.kr( freq, onsetTrig );


	// VALUE BUS SENDS
	// delay the audio signal
	// this insures that the recorders get the start of notes
	inSig = DelayC.ar( inSig, 1, onsetDelay*2 );
	delayFreq = DelayC.kr( freq, 1, onsetDelay*2 );
	Out.ar( ~micDelayed, inSig );

	// send out the chroma array
	Out.kr(~chromaBus, chroma);

	// send out the pitch estimates from AutoCorrelation and Chroma
	Out.kr(~pitchBus, [
		freq, onsetFreq, delayFreq, chromaMax,
		currentKey[1], hasFreq.lag(0.1)
	]);


	// send out note estimates
	SendReply.kr(
		trig: onsetTrig,
		cmdName: '/noteOnset',
		values: [onsetFreq.cpsmidi.round, freq],
		replyID: -1
	);

	// TEMPO ANALYSIS
	# quarter, eighth, sixt, tempo = BeatTrack.kr(
		chain: fft,
		lock: tempoLock
	);

	// bring into human readable
	tempo = (tempo * 60).lag(6).clip(20, 300);
	// tempo.poll( trig: 1, label: \tempo);
	// send out a control bus
	Out.kr( ~tempoBus, [tempo.lag(20), quarter] );






	// // test beeps
	// beep = Saw.ar( onsetFreq, mul: 0.2 );
	// beep = beep * Linen.kr(
	// 	onsetTrig,
	// 	attackTime: 0.01,
	// 	releaseTime: 0.2
	// );
	// // test signal output
	// Out.ar( outBus, beep!2 );

}).play;
);

// TEMPO UPDATE
(
~tempoTask.stop;
{
	4.0.wait;
	~tempoTask = Task({
		arg tempo = [120];
		inf.do({
			tempo = ~tempoBus.getnSynchronous(1);
			// tempo.postln;
			0.2.wait;
			if(tempo[0].isFloat, {
				// tempo[0].postln;
				TempoClock.default.tempo = (tempo[0]/60);
			}, {"tempo not working".postln;});

		});
	}).start;
}.fork;
);


(
~width = 840; ~height = 420;
~mainWin = Window("sythMusik", Rect(Window.screenBounds.width-~width, Window.screenBounds.height-~height,~width,~height)).front;
~mainWin.front;
~chromaPlot = Plotter("Chroma", Rect(10, 10, 400, 400), ~mainWin);
~chromaPlot.plotMode = \plines;
~chromaPlot.value = Array.series( 12, 0.0, 0.01);
~chromaPlot.findSpecs = false;
~chromaPlot.specs = ControlSpec(0.0, 1.0, 'linear', 0.0, 0.0, "");

~chromaPitch = (
	StaticText(~mainWin,
		Rect(430, 220, 200, 190))
	.background_(Color.fromHexString("123E40"))
	.align_(\center)
	.font_(Font("Helvetica Neue Light", 52))
	.stringColor_(Color.fromHexString("FFFFFF"))
);

~freq = (
	StaticText(~mainWin,
		Rect(430, 10, 200, 190))
	.background_(Color.fromHexString("133661"))
	.align_(\center)
	.font_(Font("Helvetica Neue Light", 52))
	.stringColor_(Color.fromHexString("FFFFFF"))
);

~pitchLabels = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

~chromaPlotTask = Task(
	{
		inf.do({
			var chroma, chromaPitch, pitch, onsetPitch, delayFreq;
			chroma = ~chromaBus.getnSynchronous(12);
			~chromaPlot.value = chroma.normalize;
			~chromaPlot.refresh;

			// chromaPitch = chroma.maxIndex;
			#pitch, onsetPitch, delayFreq, chromaPitch = ~pitchBus.getnSynchronous(4);

			~freq.string = (onsetPitch.round(0.01).asString + "\n" + ~pitchLabels[(onsetPitch.cpsmidi.round % 12).asInteger]);
			~chromaPitch.string = ~pitchLabels[chromaPitch];

			0.1.wait;
		});
	},
	clock: AppClock
).start
);

CmdPeriod.doOnce({~mainWin.close;});

)